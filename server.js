var express         = require('express');
var path            = require('path');
var log             = require('./libs/log')(module);
var objects         = require('./libs/mongoose');//mongo db
var config          = require('./libs/config');
var Product         = require('./modules/product'); //валидация для записи в бд
var Company         = require('./modules/company');//валидация для записи в бд

var app = express();
var logger = require('morgan');
var bodyParser = require('body-parser');
var Router = require('router');

var router = new Router();
app.use(logger('dev')); // выводим все запросы со статусами в консоль
app.use(bodyParser.urlencoded({extended : true}));

app.use(router); // модуль для простого задания обработчиков путей


app.get('/', function (req, res) {
    res.send(
        '<a href="/products">Add/edit/del products</a><br>' +
        '<a href="/companies">Add/edit/del companies</a>'
    );
});

//edit/del/add data
app.get('/products', function(req, res) {
    return  Product.find(function (err, product){
        if (!err) {
            var table = '<form action="/products/save" method="post" style="margin:0;">' +
            '<input type="submit" value="add">' +
            '</form>' +'<a href="/products/show">get all products</a>'+
                '<table border="1">' +
                '<tr>' +
                '<td>itemId</td>' +
                '<td>itemName</td>' +
                '<td>providerCompany </td>' +
                '<td >action</td>' +
                '</tr>';
            for( var i = 0; i < product.length; i++ )
            {
                var  _id = product[i]['_id'];
                var  itemId = product[i]['itemId'];
                var  itemName = product[i]['itemName'];
                var  providerCompany = product[i]['providerCompany'];
                table +='<tr>' +
                    '<td><a href="/products/show-one/' + itemId + '">' + itemId + '</a></td>' +
                    '<td>' + itemName + '</td>' +
                    '<td>' + providerCompany + '</td>' +
                    '<td>' +
                    '<form style="float:left;"  action="/products/delete" method="post" style="margin:0;">' +
                        '<input type="hidden" name="itemId" value="' + itemId + '">' +
                        '<input type="submit" value="del">' +
                    '</form>' +
                    '<form style="float:left;" action="/products/save" method="post" style="margin:0;">' +
                        '<input type="hidden" name="_id" value="' + _id + '">' +
                        '<input type="hidden" name="itemId" value="' + itemId + '">' +
                        '<input type="hidden" name="itemName" value="' + itemName + '">' +
                        '<input type="submit" value="edit">' +
                    '</form>' +
                    '</td>' +
                    '</tr>';
            }
            table += '</table>';
            return res.send(table);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});
app.get('/companies', function(req, res) {
    return  Company.find(function (err, company){
        if (!err) {
            var table = '<form action="/companies/save" method="post" style="margin:0;">' +
                '<input type="submit" value="add">' +
                '</form>'
                +'<a href="/companies/show">get all companies</a>'+
                '<table border="1">' +
                '<tr>' +
                '<td>companyId</td>' +
                '<td>companyName</td>' +
                '<td>providerProduct </td>' +
                '<td >action</td>' +
                '</tr>';
            for( var i = 0; i < company.length; i++ )
            {
                var  _id = company[i]['_id'];
                var  companyId = company[i]['companyId'];
                var  companyName = company[i]['companyName'];
                var  providerProduct = company[i]['providerProduct'];
                table +='<tr>' +
                    '<td><a href="/companies/show-one/' + companyId + '">' + companyId + '</a></td>' +
                    '<td>' + companyName + '</td>' +
                    '<td>' + providerProduct + '</td>' +
                    '<td>' +
                    '<form style="float:left;"  action="/companies/delete" method="post" style="margin:0;">' +
                    '<input type="hidden" name="companyId" value="' + companyId + '">' +
                    '<input type="submit" value="del">' +
                    '</form>' +
                    '<form style="float:left;" action="/companies/save" method="post" style="margin:0;">' +
                    '<input type="hidden" name="_id" value="' + _id + '">' +
                    '<input type="hidden" name="companyId" value="' + companyId + '">' +
                    '<input type="hidden" name="companyName" value="' + companyName + '">' +
                    '<input type="submit" value="edit">' +
                    '</form>' +
                    '</td>' +
                    '</tr>';
            }
            table += '</table>';
            return res.send(table);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

//show all
app.get('/products/show', function(req, res) {
    return Product.find(function (err, product){
        if (!err) {
            var result =[];
            for( var i = 0; i < product.length; i++ )
            {
                var  itemId = product[i]['itemId'];
                var  itemName = product[i]['itemName'];
                var  providerCompany = product[i]['providerCompany'];

                result [i] =
                {
                    "itemId": itemId,
                    "itemName": itemName,
                    "providerCompany": providerCompany
                };

            }
            //console.log(result);
            return res.send(result);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});
app.get('/companies/show', function(req, res) {
    return Company.find(function (err, company) {
        if (!err) {
            var result = '[';

            for( var i = 0; i < company.length; i++ )
            {
                var  companyId = company[i]['companyId'];
                var  companyName = company[i]['companyName'];
                var  providerProduct = company[i]['providerProduct'];

                result +='{companyId:' + companyId + ', companyName:' + companyName + ', ['+providerProduct+']}'+( i < company.length - 1 ? ',' : '');
            }
            result += ']';
            return res.send(result);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

//show by itemId
app.get('/products/show-one/:itemId', function(req, res) {
    return Product.find({ itemId: req.params.itemId },
        function(err, product) {
            if (!err) {
                var result =[];
                for( var i = 0; i < product.length; i++ )
                {
                    var  itemId = product[i]['itemId'];
                    var  itemName = product[i]['itemName'];
                    var  providerCompany = product[i]['providerCompany'];

                    result [i] =
                    {
                        "itemId": itemId,
                        "itemName": itemName,
                        "providerCompany": providerCompany
                    };

                }
                //console.log(result);
                return res.send(result);
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
});
app.get('/companies/show-one/:companyId', function(req, res) {
    return Company.find({ companyId: req.params.companyId },function (err, company) {
        if (!err) {
                var  companyId = company[0]['companyId'];
                var  companyName = company[0]['companyName'];
                var  providerProduct = company[0]['providerProduct'];

            var result = '[{companyId:' + companyId + ', companyName:' + companyName + ', ['+providerProduct+']}]';

            return res.send(result);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s',res.statusCode,err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

//save and create
app.post('/products/save', function(req, res) {
    var _id = req.body._id ? req.body._id : 'null' ;
    var itemId = req.body.itemId ? req.body.itemId : 'null';
    var itemName = req.body.itemName ? req.body.itemName : 'null';
    var providerCompany = req.body.providerCompany ? req.body.providerCompany : 'null';
    // форма для редактирования
    if(  _id !== 'null'  && itemId !== 'null'  && itemName !== 'null' && providerCompany == 'null' )
    {
        console.log('1');
        return Company.find(function (err, company) {
            if (!err) {
                var result = '';

                for( var i = 0; i < company.length; i++ )
                {
                    var  companyName = company[i]['companyName'];
                    result +='<option value="' + companyName + '">' + companyName + '</option>';
                }

                var form = '<form action="/products/save" method="post">' +
                    '<p  >itemId<br><input type="number" value="' + itemId + '" name="itemId"  required style="width:50px;"  min="0"></p>' +
                    '<p>itemName<br><input type="text"  value="' + itemName + '" required name="itemName" min="0"></p>'  +
                    '<p>providerCompany<br><select name="providerCompany[]" required multiple size="3"> <option value="none">none</option>' + result +
                    '</select></p>'+
                    '<input type="hidden" name="_id" value="' + _id + '">'+
                    '<input type="submit" value="edit">'+
                    '</form>';

                return res.send(form);
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }
    // форма для создания
    else if(  _id == 'null' && itemId == 'null'  && itemName == 'null' && providerCompany == 'null' )
    {
        console.log('2');
        return Company.find(function (err, company) {
            if (!err) {
                var result = '';

                for( var i = 0; i < company.length; i++ )
                {
                    var  companyName = company[i]['companyName'];
                    result +='<option value="' + companyName + '">' + companyName + '</option>';
                }

                var form = '<form action="/products/save" method="post">' +
                    '<p  >itemId<br><input type="number" name="itemId"  required style="width:50px;"  min="0"></p>' +
                    '<p>itemName<br><input type="text"  required name="itemName" min="0"></p>'  +
                    '<p>providerCompany<br><select name="providerCompany[]" required multiple size="3"> <option value="none">none</option>' + result +
                    '</select></p>'+
                    '<input type="submit" value="add">'+
                    '</form>';

                return res.send(form);
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }
    //редактирование
    else if( _id !== 'null' && itemId !== 'null'  && itemName !== 'null' && providerCompany !== 'null' )
    {
        console.log('3');
        return Product.findOneAndUpdate({ _id: _id },
            {
                itemId: itemId,
                itemName: itemName,
                providerCompany: providerCompany
            },function(err,product) {

                if (!err) {
                    return res.send('Product edit! <a href="/products">Add/edit/del products</a>');
                } else {
                    res.statusCode = 500;
                    log.error('Internal error(%d): %s',res.statusCode,err.message);
                    return res.send({ error: 'Server error' });
                }
            });
    }
    //создание
    else if( req.body.itemId !== 'null'  && req.body.itemName !== 'null' && req.body.providerCompany !== 'null' )
    {
        console.log('4');
        var newProduct = new Product(
            {
                itemId: itemId,
                itemName: itemName,
                providerCompany: providerCompany
            });

        newProduct.save(function(err,product)
        {
            if (!err) {
                return res.send('Product create! <a href="/products">Add/edit/del products</a>');
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }
});
app.post('/companies/save', function(req, res) {
    var _id = req.body._id ? req.body._id : 'null' ;
    var companyId = req.body.companyId ? req.body.companyId : 'null';
    var companyName = req.body.companyName ? req.body.companyName : 'null';
    var providerProduct = req.body.providerProduct ? req.body.providerProduct : 'null';
    // форма для редактирования
    if(  _id !== 'null'  && companyId !== 'null'  && companyName !== 'null' && providerProduct == 'null' )
    {
        console.log('1');
        return Product.find(function (err, product) {
            if (!err) {
                var result = '';

                for( var i = 0; i < product.length; i++ )
                {
                    var  itemName = product[i]['itemName'];
                    result +='<option value="' + itemName + '">' + itemName + '</option>';
                }

                var form = '<form action="/companies/save" method="post">' +
                    '<p  >companyId<br><input type="number" value="' + companyId + '" name="companyId"  required style="width:50px;"  min="0"></p>' +
                    '<p>companyName<br><input type="text"  value="' + companyName + '" required name="companyName" min="0"></p>'  +
                    '<p>providerProduct<br><select name="providerProduct[]" required multiple size="3"> <option value="none">none</option>' + result +
                    '</select></p>'+
                    '<input type="hidden" name="_id" value="' + _id + '">'+
                    '<input type="submit" value="edit">'+
                    '</form>';

                return res.send(form);
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }
    // форма для создания
    else if(  _id == 'null' && companyId == 'null'  && companyName == 'null' && providerProduct == 'null' )
    {
        console.log('2');
        return Product.find(function (err, product) {
            if (!err) {
                var result = '';

                for( var i = 0; i < product.length; i++ )
                {
                    var  itemName = product[i]['itemName'];
                    result +='<option value="' + itemName + '">' + itemName + '</option>';
                }

                var form = '<form action="/companies/save" method="post">' +
                    '<p>companyId<br><input type="number" name="companyId"  required style="width:50px;"  min="0"></p>' +
                    '<p>companyName<br><input type="text"  required name="companyName" min="0"></p>'  +
                    '<p>providerProduct<br><select name="providerProduct[]" required multiple size="3"> <option value="none">none</option>' + result +
                    '</select></p>'+
                    '<input type="submit" value="add">'+
                    '</form>';

                return res.send(form);
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }
    //редактирование
    else if( _id !== 'null' && companyId !== 'null'  && companyName !== 'null' && providerProduct !== 'null' )
    {
        console.log('3');
        return Company.findOneAndUpdate({ _id: _id },
            {
                companyId: companyId,
                companyName: companyName,
                providerProduct: providerProduct
            },function(err,product) {

                if (!err) {
                    return res.send('Company edit! <a href="/companies">Add/edit/del companies</a>');
                } else {
                    res.statusCode = 500;
                    log.error('Internal error(%d): %s',res.statusCode,err.message);
                    return res.send({ error: 'Server error' });
                }
            });
    }
    //создание
    else if( companyId !== 'null'  && companyName !== 'null' && providerProduct !== 'null' )
    {
        console.log('4');
        var newCompany = new Company(
            {
                companyId: companyId,
                companyName: companyName,
                providerProduct: providerProduct
            });

        newCompany.save(function(err,company)
        {
            if (!err) {
                return res.send('Company create! <a href="/companies">Add/edit/del companies</a>');
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }
});

//delete by itemId
app.post('/products/delete/', function(req, res) {
    var itemId = req.body.itemId ? req.body.itemId : 'null';
    if( itemId !== 'null')
    {
        return Product.findOneAndRemove({ itemId: itemId }, function(err,product) {

            if (!err) {
                return res.send('Product deleted! <a href="/products">Add/edit/del products</a>');
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    }


});
app.post('/companies/delete/', function(req, res) {

    var companyId = req.body.companyId ? req.body.companyId : 'null';
    if( companyId !== 'null') {
        return Company.findOneAndRemove({companyId: companyId}, function (err, company) {
            if (!err) {
                return res.send('Company deleted! <a href="/companies">Add/edit/del companies</a>');
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({error: 'Server error'});
            }
        });
    }
});

//errors
app.get('/ErrorExample', function(req, res, next){
    next(new Error('Random error!'));
});
app.listen(1337, function(){
    console.log('Express server listening on port 1337');
});
app.listen(config.get('port'), function(){
    log.info('Express server listening on port ' + config.get('port'));
});
app.use(function(req, res, next){
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
    return;
});
app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});

