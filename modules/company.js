
var mongoose = require('mongoose');

var companySchema = mongoose.Schema(
    {
        companyId:{
            type:Number,
            unique:true,
            required:true
        },
        companyName:{
            type:String,
            unique:true,
            required:true
        },
        providerProduct: {
            type:Array,
            required:true
        }
    });

// we need to create a model using it
var Companies = mongoose.model('company', companySchema);

// make this available to our users in our Node applications
module.exports = Companies;