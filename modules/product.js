var mongoose = require('mongoose');
//
var productSchema = mongoose.Schema(
    {
        itemId:{
            type:Number,
            unique:true,
            required:true
        },
        itemName:{
            type:String,
            unique:true,
            required:true
        },
        providerCompany: {
            type:Array,
            required:true
        }
    });

// we need to create a model using it
var Products = mongoose.model('product', productSchema);

// make this available to our users in our Node applications
module.exports = Products;